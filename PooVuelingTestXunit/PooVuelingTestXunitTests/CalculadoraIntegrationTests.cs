﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PooVuelingTestXunit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PooVuelingTestXunit.Tests
{
  
    [TestClass()]
    public class CalculadoraIntegrationTests
    {
        ICalculadora Calculadora = new Calculadora();
        [DataRow(4,2,2)]
        [DataRow(9,3,3)]
        [TestMethod]
        public void DivisionTest(int num1 , int num2 , int resultado)
        {
            Assert.IsTrue(Calculadora.Division(num1,num2)==resultado);
        }

        [DataRow(2, 2, 4)]
        [DataRow(3, 4, 12)]
        [TestMethod]
        public void MultiplicacionTest(int num1 , int num2 , int resultado)
        {
            Assert.IsTrue(Calculadora.Multiplicacion(num1, num2) == resultado);
        }

        [DataRow(2, 2, 0)]
        [DataRow(9, 3, 6)]
        [TestMethod]
        public void RestaTest(int num1, int num2, int resultado)
        {
            Assert.IsTrue(Calculadora.Resta(num1, num2) == resultado);
        }

        [DataRow(2, 2, 4)]
        [DataRow(5, 3, 8)]
        [TestMethod]
        public void SumaTest(int num1, int num2, int resultado)
        {
            Assert.IsTrue(Calculadora.Suma(num1, num2) == resultado);
        }
    }
}