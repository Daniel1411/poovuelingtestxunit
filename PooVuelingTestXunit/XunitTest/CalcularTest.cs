﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using PooVuelingTestXunit;

namespace XunitTest
{
    
    public class CalcularTest
    {
        ICalculadora _calculadora = new Calculadora();


        [Fact]
        public void Divison()
        {
            Assert.Equal(1,_calculadora.Division(2, 2));
        }

        [Fact]
        public void Multiplicacion()
        {
            Assert.Equal(4, _calculadora.Multiplicacion(2, 2));
        }

        [Fact]
        public void Resta()
        {
            Assert.Equal(0, _calculadora.Resta(2, 2));
        }

        [Fact]
        public void Suma()
        {
            Assert.Equal(4, _calculadora.Suma(2, 2));
        }

    }
}
